from target_hotglue.client import HotglueSink
from time import time
import requests
from datetime import datetime, timedelta
from backports.cached_property import cached_property


class ZohobooksV2Sink(HotglueSink):
    access_token = None
    expires_at = None
    # base_url = "https://books.zoho.com/api/v3/" @TODO figure out when was this endpoint changed?
    total = 0

    @cached_property
    def base_url(self) -> str:
        url = self.config.get("accounts-server", "https://accounts.zoho.com")
        api_url = "https://www.zohoapis.com/books/v3/"
        # Mapping domain suffixes to their corresponding base API URIs
        domain_mapping = {
            '.com': 'https://www.zohoapis.com/books/',
            '.eu': 'https://www.zohoapis.eu/books/',
            '.in': 'https://www.zohoapis.in/books/',
            '.com.au': 'https://www.zohoapis.com.au/books/',
            '.jp': 'https://www.zohoapis.jp/books/'
        }

        # Check for domain presence and update api_url dynamically
        for domain, base_api_url in domain_mapping.items():
            if domain in url:
                api_url = base_api_url + 'v3/'  # Append '/v3/' to the base URL
                break  # Stop checking further domains if found

        return api_url

    def get_auth(self):
        url = self.config.get("accounts-server", "https://accounts.zoho.com")
        if self.access_token is None or self.expires_at <= datetime.utcnow():
            response = requests.post(
                f"{url}/oauth/v2/token",
                data={
                    "client_id": self.config.get("client_id"),
                    "client_secret": self.config.get("client_secret"),
                    "refresh_token": self.config.get("refresh_token"),
                    "grant_type": "refresh_token",
                },
            )

            data = response.json()
            if data.get("error"):
                raise Exception(f"Auth request failed with response {response.text}")

            self.access_token = data["access_token"]

            self.expires_at = datetime.utcnow() + timedelta(
                seconds=int(data["expires_in"]) - 10
            )  # pad by 10 seconds for clock drift

        return self.access_token

    def log_request_response(self, record, response):
        self.logger.info(f"Sending payload for stream {self.name}: {record}")
        self.logger.info(f"Response: {response.text}")

    def entity_search(self, entity_name="contacts", params=None):
        url = f"{self.base_url}{entity_name}"
        res = requests.get(url=url, params=params, headers=self.get_headers()).json()
        if entity_name in res:
            if len(res[entity_name]) > 0:
                return res[entity_name]
            else:
                return []
        else:
            return []

    def entity_post(self, entity_name, payload):
        url = f"{self.base_url}/{entity_name}"
        res = requests.post(url, headers=self.get_headers(), json=payload)
        return res

    def get_headers(self):
        headers = {}
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = f"Bearer {self.get_auth()}"
        return headers

    def upsert_record(self, record: dict, context: dict):
        method = "POST"
        state_dict = dict()
        id = None
        if "error" in record:
            state_dict["success"] = False
            state_dict["message"] = record["error"]
            return None, False, state_dict

        endpoint = self.endpoint
        if "id" in record:
            id = record["id"]
            endpoint = f"{self.endpoint}/{record['id']}"
            del record["id"]
            method = "PUT"

        response = self.request_api(
            http_method=method,
            request_data=record,
            endpoint=endpoint,
            headers=self.get_headers(),
        )
        self.log_request_response(record, response)
        if response.status_code in [200, 201]:
            state_dict["success"] = True
            id = response.json().get("id")
        elif response.status_code == 204 and method == "PUT":
            state_dict["is_updated"] = True
        return id, response.ok, state_dict
