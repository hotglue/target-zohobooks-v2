"""ZohobooksV2 target class."""

from target_hotglue.target import TargetHotglue
from singer_sdk import typing as th

from target_zohobooks_v2.sinks import InvoicesSink, BuyOrdersSink


class TargetZohobooksV2(TargetHotglue):
    """Sample target for ZohobooksV2."""

    SINK_TYPES = [InvoicesSink, BuyOrdersSink]
    name = "target-zohobooks-v2"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "filepath", th.StringType, description="The path to the target output file"
        ),
        th.Property(
            "file_naming_scheme",
            th.StringType,
            description="The scheme with which output files will be named",
        ),
    ).to_dict()


if __name__ == "__main__":
    TargetZohobooksV2.cli()
