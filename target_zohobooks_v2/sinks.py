"""ZohobooksV2 target sink class, which handles writing streams."""


from target_zohobooks_v2.client import ZohobooksV2Sink
from target_zohobooks_v2.mapping import UnifiedMapping
from datetime import datetime, timedelta


class InvoicesSink(ZohobooksV2Sink):
    """ZohobooksV2 target sink class."""

    name = "Invoices"
    endpoint = "invoices"

    def invoice_lookup(self, payload):
        # date format fixes
        created_date = datetime.strptime(
            payload["date"], "%Y-%m-%dT%H:%M:%SZ"
        ).strftime("%Y-%m-%d")
        last_modified_time = datetime.strptime(
            payload["last_modified_time"], "%Y-%m-%dT%H:%M:%SZ"
        ).strftime("%Y-%m-%d")
        due_date = datetime.strptime(
            payload["due_date"], "%Y-%m-%dT%H:%M:%SZ"
        ).strftime("%Y-%m-%d")
        payload.update(
            {
                "date": created_date,
                "last_modified_time": last_modified_time,
                "due_date": due_date,
            }
        )
        # line items
        lineitems = payload["line_items"]
        new_lineItems = []
        # lookup item_id
        for lineitem in lineitems:
            new_item = lineitem
            if "item_id" not in lineitem:
                item = self.entity_search("items", {"name": lineitem["name"]})
                if len(item) > 0:
                    item = item[0]
                    new_item.update(
                        {
                            "item_id": item["item_id"],
                        }
                    )
            new_lineItems.append(new_item)
        payload["line_items"] = new_lineItems
        # check contact_id
        if "customer_id" not in payload:
            customer = self.entity_search(
                "contacts", {"contact_name": payload["customer_name"]}
            )
            if len(customer) > 0:
                customer = customer[0]
                payload.update({"customer_id": customer["contact_id"]})

        return payload

    def preprocess_record(self, record: dict, context: dict) -> dict:
        """Process the record."""
        mapping = UnifiedMapping()
        payload = mapping.prepare_payload(record, "invoices")
        payload = self.invoice_lookup(payload)
        if "invoice_id" in payload:
            payload.update({"id": payload["invoice_id"]})
        if "status" in payload:
            payload["status"] = payload["status"].lower()
        return payload


class BuyOrdersSink(ZohobooksV2Sink):
    """ZohobooksV2 target sink class."""

    name = "BuyOrders"
    endpoint = "purchaseorders"

    def preprocess_record(self, record: dict, context: dict) -> dict:
        """Process the record."""
        mapping = UnifiedMapping()
        # get product ids for lines

        payload = mapping.prepare_payload(record, "buy_orders")
        line_items = [item for item in payload.get("line_items") if item.get("item_id")]
        if not line_items:
            self.logger.info(f"skipping buyorder {vendor_name} with no")
            return
        else:
            payload["line_items"] = line_items

        # get vendor id
        vendor_name = record.get("supplier_name")
        if vendor_name:
            vendors = self.entity_search(
                "contacts", {"contact_name": record.get("supplier_name")}
            )
        if vendors:
            vendor_id = vendors[0]["contact_id"]
            payload["vendor_id"] = vendor_id
        else:
            return {
                "error": f"Supplier with name={vendor_name} does not exist in zohobooks"
            }
        return payload
