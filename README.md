# target-zohobooks-v2

`target-zohobooks-v2` is a Singer target for Zohobooks uses Hotglue's target-sdk.

## Installation


```bash
pipx install target-zohobooks-v2
```

## Configuration

### Accepted Config Options


```bash
{
  "client_id":"Zohobooks App Client Id",
  "client_secret":"Zohobooks App Client Secret",
  "access_token": "1000.6090da42b71c53321b13bd76d8b1f795.fc7a49d4aac3fb4afdf8646dd1840c9c",
  "refresh_token": "Zohobooks App Client Refresh Token",
  "accounts-server": "https://accounts.zoho.com" # Zoho region's account server. Optional
  
}
```

```bash
target-zohobooks-v2 --about
```

### Configure using environment variables

This Singer target will automatically import any environment variables within the working directory's
`.env` if the `--config=ENV` is provided, such that config values will be considered if a matching
environment variable is set either in the terminal context or in the `.env` file.


### Executing the Target Directly

```bash
target-zohobooks-v2 --version
target-zohobooks-v2 --help
# Test using the "Carbon Intensity" sample:
tap-carbon-intensity | target-zohobooks-v2 --config /path/to/target-zohobooks-v2-config.json
```

## Developer Resources


### Initialize your Development Environment

```bash
pipx install poetry
poetry install
```

### Create and Run Tests

Create tests within the `target_zohobooks_v2/tests` subfolder and
  then run:

```bash
poetry run pytest
```

You can also test the `target-zohobooks-v2` CLI interface directly using `poetry run`:

```bash
poetry run target-zohobooks-v2 --help
```
